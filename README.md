Role Name
=========

A role that installs the MEI Generator docker stack

Role Variables
--------------

The most important variables are listed below:

``` yaml
mei_generator_compose_dir: '/srv/mei_generator_stack'
mei_generator_docker_stack_name: 'mei-generator'
mei_generator_compose_url: 'https://raw.githubusercontent.com/CMCC-Foundation/bluecloud-config/master/docker-compose.yml'
cmmc_mysql_root_password: 'set it in a vault file'
cmmc_mysql_password: 'set it in a vault file'
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
